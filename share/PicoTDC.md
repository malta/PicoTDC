# PicoTDC {#PackagePicoTDC}

PicoTDC is a package for the control and readout of the PicoTDC ASIC
https://doi.org/10.1088/1748-0221/18/07/P07012

## Applications

- MaltaPicoTDC: tool to run PicoTDC stand-alone
- MaltaPicoAnalog: create a PicoTDCModule and run it in stand-alone while a MaltaAnlogScan runs in the background

## Libraries

- PicoTDC: Readout and control of PicoTDC

### PicoTDC library classes

- PicoTDC: control and readout the PicoTDC through ipbus
- PicoTDCData: tool to encode/decode data of PicoTDC
- PicoTDCTree: tool to read/write PicoTDCData into ROOT trees
- PicoTDCModule: ReadoutModule for MaltaMultiDAQ to control PicoTDC
