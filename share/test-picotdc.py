#!/usr/bin/env python
import os
import sys
import time
import argparse
import signal
import PyPicoTDC

g_cont=True
def handler(signum,frame):
    global g_cont
    print ("You pressed ctrl-C to exit")
    g_cont=False
    pass

parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--connstr',help='ipbus connection string',default="udp://ep-ade-gw-07:60000")
args=parser.parse_args()

p=PyPicoTDC.PicoTDC()
p.Connect(args.connstr)
#p.Configure()

##tester.startup()
p.Init();
#p.Startup();
#p.SetTotModeTop()
p.EnableChannel(1,True)
p.EnableChannel(31,True)
#p.EnableChannel(31)
p.EnableTrigger(True)
#p.SetTriggerLatency(13)
#p.SetTriggerWindow(10)
#p.Reset()
#tester.picoTDC_readout.read_all_TOT_hist("/home/sbmuser/PicoTDC_files/Test_Beam/RUN1049",0)
signal.signal(signal.SIGINT,handler)
signal.signal(signal.SIGTERM,handler)
signal.signal(signal.SIGILL,handler)
#words=p.Read()
#print (words)

led=True
while True:
    if not g_cont: break
    led=not led
    p.Blink(1,led)
    p.Blink(2,led)
    p.Blink(3,led)
    p.Blink(4,led)
    p.Blink(5,led)
    words=p.ReadData()
    if len(words) > 0:
    	print("Number of words read: %i" % len(words))
    	for word in words:
        	print word
        	pass
    	pass

print("Have a nice day")
 
