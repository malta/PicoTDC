#!/usr/bin/env python
import os
import sys
import time
import argparse
import signal
import PyPicoTDC

g_cont=True
def handler(signum,frame):
    global g_cont
    print ("You pressed ctrl-C to exit")
    g_cont=False
    pass

parser=argparse.ArgumentParser()
parser.add_argument('-c' ,'--connstr',help='ipbus connection string',default="udp://ep-ade-gw-07:60000")
args=parser.parse_args()

p=PyPicoTDC.PicoTDC()
p.Connect(args.connstr)

p.Init();
p.Startup()
time.sleep(1)
p.SetTotModeTop()
time.sleep(1)
p.SetTotModeBot()
time.sleep(1)
p.EnableChannel(31, True)
time.sleep(1)

p.EnableChannel(1, True)
time.sleep(1)

p.EnableChannel(35, True)
time.sleep(1)

p.EnableChannel(48, True)
time.sleep(1)

p.EnableTrigger(1)
time.sleep(1)
p.SetTriggerLatency(17)
time.sleep(1)

p.SetTriggerWindow(10)
time.sleep(1)


p.Reset()

signal.signal(signal.SIGINT,handler)
signal.signal(signal.SIGTERM,handler)
signal.signal(signal.SIGILL,handler)

while True: 
    time.sleep(1)
    if not g_cont: break
    words=p.ReadData()

    print("Number of words read: %i" % len(words))
    for word in words:
        print word
        #print ("0x%X"%word)
        pass
    pass

print("Have a nice day")
 
