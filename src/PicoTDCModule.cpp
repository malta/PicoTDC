#include "PicoTDC/PicoTDCModule.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <unistd.h>

#include "TH2D.h"
#include "TCanvas.h"

DECLARE_READOUTMODULE(PicoTDCModule)

using namespace std;
/*
bool m_cont=true; 

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  m_cont=false;
}
*/

//VD hack:


PicoTDCModule::PicoTDCModule(string name, string address, string outdir): 
  ReadoutModule(name,address,outdir){  
  

  m_pico = new PicoTDC();
  m_pico->Connect(address);
  m_ntuple = 0;
  ostringstream os;
  os << m_name << "_timing";
  m_Htime=new TH1D(os.str().c_str(),"timing;time since L1A [ns]",256,0,200); 
  os.str("");
  os << m_name << "_nPixel";
  m_Hsize=new TH1D(os.str().c_str(),"pixel;# pixel per event",6,-0.5,5.5); 
  os.str("");
  os << m_name << "_hitMap";
  m_Hmap =new TH2D(os.str().c_str(),"hitMap;pix X;pix Y",512,0,512,512,0,512);
  
  m_internalTrigger=false;

}

PicoTDCModule::~PicoTDCModule(){
  delete m_pico;
  if(m_ntuple) delete m_ntuple;
}

void PicoTDCModule::Configure(){

  m_pico->Init();
  m_pico->Startup();
  usleep(1000000);		    //sleep 1s needed for the configuration of pico 
  m_pico->SetTotModeTop();
  usleep(1000000);
  m_pico->SetTotModeBot();
  usleep(1000000);
  m_pico->EnableChannel(31, true);  //hardcoded
  usleep(1000000);
  m_pico->EnableChannel(1, true);   //hardcoded
  usleep(1000000);
  m_pico->EnableChannel(35, true);  //hardcoded
  usleep(1000000);
  m_pico->EnableChannel(48, true);  //hardcoded
  usleep(1000000);
  m_pico->EnableTrigger(1);
  usleep(1000000);
  m_pico->configFromFile("pico.txt");
  //m_pico->SetTriggerLatency(17);    //hardcoded
  //usleep(1000000);
  //m_pico->SetTriggerWindow(10);     //hardcoded 
  usleep(1000000);
  m_pico->Reset();
  usleep(1000000);
  

/*
  cout << m_name << ": Configure" << endl;
    
  ReadoutConfig * config=GetConfig();

  if(config){
    for(uint32_t i=0;i<config->GetKeys().size();i++){
      string str=config->GetKeys().at(i);
      cout << "Parse: " << str << endl;
      size_t pos1=str.find(":");
      if(pos1!=string::npos){
	string key=str.substr(0,pos1);
	string sval=str.substr(pos1+1);
	uint32_t val=stoi(sval);
	cout << key << ": " << val << endl;
	m_pico->SetValue(key,val);
      }
    }
  }
  
  m_pico->Configure();
*/
}

void PicoTDCModule::Start(string run, bool usePath){
  string fname;
  cout << m_name << ": Start run " << run << endl;
  if(!usePath){
    cout << m_name << ": Start run " << run << endl;
    ostringstream os;
    os << m_outdir << "/"  << run << ".root";
    fname = os.str();
  }
  else{
    fname = run;
  }
      
  //Create ntuple
  cout << m_name << ": Create ntuple" << endl;
  if(m_ntuple) delete m_ntuple;
  m_ntuple = new PicoTDCTree();
  m_ntuple->Open(fname,"RECREATE");
  m_ntuple->SetScaleFactorTOT(m_pico->GetScaleFactor()); 
  cout << m_name << ": Prepare for run"<< endl;
  m_pico->Reset();
  
  cout << m_name << ": Create data acquisition thread" << endl;
  m_cont=true;
  m_thread = thread(&PicoTDCModule::Run,this);
  m_running = true;
  cout << m_name << ": Data acquisition thread running" << endl;
   

}
void PicoTDCModule::Stop(){
  cout << m_name << ": Stop" << endl;
  m_cont=false;
  cout << m_name << ": Join data acquisition thread" << endl;
  m_thread.join();
  m_running=false;
  cout << m_name << ": Close tree" << endl;
  m_ntuple->GetFile()->WriteObject(m_Htime,"Timing");
  m_ntuple->GetFile()->WriteObject(m_Hsize,"NHit"  );
  m_ntuple->GetFile()->WriteObject(m_Hmap ,"HitMap");
  m_ntuple->Close();
}

void PicoTDCModule::Run(){
  vector<PicoTDCData> data;
  cout << m_name << ": Read data" << endl;
  while(m_cont){
    usleep(100);
    if ( m_resetL1Counters){
     m_pico->ResetCounters();   
     //m_pico->Reset();   
     usleep(20000);
     m_ntuple->ResetCounters();
     m_resetL1Counters=false;

    }
    data.clear();
    m_pico->ReadData(data);
    for(unsigned int i=0; i<data.size(); i++){
      m_ntuple->Set(data.at(i), m_ntuple); //it was a pointer
    }
    //if (data.size() > 0 ) Sync();
    //m_Htime->Draw("HIST");
    //if (m_resetL1Counters){
     //m_pico->ResetCounters();   
    // m_pico->Reset();   
    // usleep(20000);
    // m_ntuple->ResetCounters();
    // m_resetL1Counters=false;

    //}


  }
  //if (m_cont) Stop();
}

void PicoTDCModule::Sync(){
   m_pico->Sync();
}
 

uint32_t PicoTDCModule::GetNTriggers(){return 0;}

void PicoTDCModule::EnableFastSignal(){}

void PicoTDCModule::DisableFastSignal(){}


std::vector<TH1D*>* PicoTDCModule::GetRealTimeHistograms(){

  return m_ntuple->RetrieveHistograms();;

}



double PicoTDCModule::findMinBin(TH1D* h){

  for (int i =1+5; i<h->GetNbinsX ()-10; ++i){
   if (h->GetBinContent(i)!=0) return h->GetBinCenter(i-5) ; 
  } 
  return h->GetBinCenter(1);
}


double PicoTDCModule::findMaxBin(TH1D* h){

  for (int i =h->GetNbinsX ()-10 ;i>1; --i){
   if (h->GetBinContent(i)!=0) return h->GetBinCenter(i+5) ;
  }
  return h->GetBinCenter( h->GetNbinsX ());

}


void PicoTDCModule::PlotCanvasRealTime(TCanvas *c){

  std::vector<TH1D*>* v = GetRealTimeHistograms();
  for (unsigned int i =0 ; i < v->size()/3; ++i){
    c->cd(i+1);
    if ( ((v->at(i*3)->Integral())> 0) && (v->at(i*3)->GetBinContent(1) / v->at(i*3)->Integral())> 0.01) {
      v->at(i*3)->SetFillColor(2);
    } 
    v->at(i*3)->Draw("hist");
    c->cd(i+1+4);
    v->at(i*3+1)->GetXaxis()->SetRangeUser(findMinBin(v->at(i*3+1)), findMaxBin(v->at(i*3+1)));
    v->at(i*3+1)->Draw("hist");
    c->cd(i+1+4+4);
    v->at(i*3+1+1)->GetXaxis()->SetRangeUser(findMinBin(v->at(i*3+1+1)), findMaxBin(v->at(i*3+1+1)));
    v->at(i*3+1+1)->Draw("hist");
  }
  c->Update();

}
bool PicoTDCModule::RetrieveL1ResetFlag(){ return m_resetL1Counters; }
void PicoTDCModule::ResetL1Counter() { m_resetL1Counters= true; }
