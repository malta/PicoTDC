//! -*-C++-*-
//#include "PicoTDC/PicoTDCTree.h"
//#include "PicoTDC/PicoTDC.h"
//#include "PicoTDC/PicoTDCModule.h"
//#include "PicoTDC/PicoTDCAnalysis.h"
//#include "PicoTDC/PicoTDCData.h"
#include <cmdl/cmdargs.h>
#include "MaltaDAQ/ModuleLoader.h"
#include "TSysEvtHandler.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include "TCanvas.h"
#include "TH1D.h"
#include "TApplication.h"

using namespace std;
bool m_cont=true;

int main(int argc, char* argv[]){


  if (argc == 1) {
    cout << "column number needed exit(0)" << endl;
    exit(0);
  }  

 
  string col_number =  argv[1] ;
  
  //PicoTDCModule *m_picoModule = new PicoTDCModule ("pico", "udp://ep-ade-gw-01:60000", "blahblah");

  ReadoutModule* /*PicoTDCModule*/ m_picoModule = NULL; 
  m_picoModule =  ModuleLoader::newReadoutModule("PicoTDC","PicoTDCModule",
							  "PicoTDC",
							  "udp://ep-ade-gw-01:60000", "pico_runs");
  m_picoModule->Configure();

  //for (int i =0; i < 503; ++i){
  for (int i =0; i < 25; ++i){
    string delta= to_string(504-i*20);
    //string delta= to_string(504-i);
    string yy = to_string(i*20);
    //string yy = to_string(i);
    m_picoModule->Start(("run_col"+col_number+"_"+yy+".root").c_str(), false);
    system(("MaltaAnalogScan -t 10000 -l "+delta+" -f picoTest -o Plane1 -r "+col_number+" 1 "+yy+" 1 -a udp://ep-ade-gw-01:50001 -c configs/config_pico.txt -y -E").c_str());
    m_picoModule->Stop();
  }

  cout << "Hello World!" << endl;

  return 0;
}
