#include "PicoTDC/PicoTDC.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <bitset>
#include <fstream>
#include <string>

using namespace std;
using namespace ipbus;

PicoTDC::PicoTDC(){
  
  m_verbose=true;
  m_verbose2=false;
  m_ipb_pico=0;
  
  if(m_verbose){ cout << "Create PicoTDC registers" << endl;}

  registers.push_back(new Register("magic_word",0x0004,0x0,8,0x5C));
  registers.push_back(new Register("digital_reset",0x0004,0x8,1,0x0));
  registers.push_back(new Register("bunchcount_reset",0x0004,0x9,1,0x0));
  registers.push_back(new Register("eventid_reset",0x0004,0xA,1,0x0));
  registers.push_back(new Register("dropcnts_reset",0x0004,0xB,1,0x0));
  registers.push_back(new Register("errorcnts_reset",0x0004,0xC,1,0x0));
  registers.push_back(new Register("falling_en",0x0008,0x1,1,0x0));
  registers.push_back(new Register("single_readout_en",0x0008,0x2,1,0x0));
  registers.push_back(new Register("highres_en",0x0008,0x5,1,0x1));
  registers.push_back(new Register("dig_rst_ext_en",0x0008,0x6,1,0x0));
  registers.push_back(new Register("bx_rst_ext_en",0x0008,0x7,1,0x1));
  registers.push_back(new Register("eid_rst_ext_en",0x0008,0x8,1,0x0));
  registers.push_back(new Register("crossing_en",0x0008,0x11,1,0x1));
  registers.push_back(new Register("rx_en_extref",0x0008,0x12,1,0x0));
  registers.push_back(new Register("rx_en_bxrst",0x0008,0x13,1,0x1));
  registers.push_back(new Register("rx_en_eidrst",0x0008,0x14,1,0x0));
  registers.push_back(new Register("rx_en_trigger",0x0008,0x15,1,0x1));
  registers.push_back(new Register("rx_en_reset",0x0008,0x16,1,0x1));
  registers.push_back(new Register("untriggered",0x000C,0x0,1,0x1));
  registers.push_back(new Register("trigger_create",0x000C,0x1,1,0x0));
  registers.push_back(new Register("relative",0x000C,0x2,1,0x1));
  registers.push_back(new Register("second_header",0x000C,0x3,1,0x0));
  registers.push_back(new Register("full_events",0x000C,0x4,1,0x0));
  registers.push_back(new Register("header_fields0",0x000C,0x8,3,0x2));
  registers.push_back(new Register("header_fields1",0x000C,0xC,3,0x1));
  registers.push_back(new Register("header_fields2",0x000C,0x10,3,0x0));
  registers.push_back(new Register("header_fields3",0x000C,0x14,3,0x0));
  registers.push_back(new Register("trigger_latency_low",0x000C,0x18,8,0x00));
  registers.push_back(new Register("trigger_latency_high",0x0010,0x0,5,0x00));
  registers.push_back(new Register("trigger_window",0x0010,0x8,0xD,0x00));
  registers.push_back(new Register("max_eventsize",0x0010,0x18,8,0x00));
  registers.push_back(new Register("tot",0x0014,0x0,1,0x0));
  registers.push_back(new Register("tot_8bits",0x0014,0x1,1,0x0));
  registers.push_back(new Register("tot_startbit",0x0014,0x2,5,0x01));
  registers.push_back(new Register("tot_leadingstartbit",0x0014,0x8,4,0x00));
  registers.push_back(new Register("bunchcount_overflow",0x0018,0x0,0xD,0x1FFF));
  registers.push_back(new Register("bunchcount_offset",0x0018,0x10,0xD,0x0000));
  registers.push_back(new Register("eventid_overflow",0x001C,0x0,0xD,0x1FFF));
  registers.push_back(new Register("eventid_offset",0x001C,0x10,0xD,0x0000));
  registers.push_back(new Register("channel_buffer_size",0x0120,0x0,4,0x7));
  registers.push_back(new Register("readout_buffer_size",0x0120,0x4,4,0x7));
  registers.push_back(new Register("trigger_buffer_size",0x0120,0x8,4,0x7));
  registers.push_back(new Register("disable_ro_reject",0x0120,0xC,1,0x1));
  registers.push_back(new Register("errorcnts_saturate",0x0120,0xD,1,0x1));
  registers.push_back(new Register("max_grouphits",0x0120,0x10,8,0xFF));
  registers.push_back(new Register("hrx_top_delay",0x0124,0x0,4,0x0));
  registers.push_back(new Register("hrx_top_bias",0x0124,0x4,4,0xF));
  registers.push_back(new Register("hrx_top_filter_trailing",0x0124,0x8,1,0x0));
  registers.push_back(new Register("hrx_top_filter_leading",0x0124,0x9,1,0x0));
  registers.push_back(new Register("hrx_top_en_r",0x0124,0xA,1,0x0));
  registers.push_back(new Register("hrx_top_en",0x0124,0xB,1,0x1));
  registers.push_back(new Register("hrx_bot_delay",0x0124,0xC,4,0x0));
  registers.push_back(new Register("hrx_bot_bias",0x0124,0x10,4,0xF));
  registers.push_back(new Register("hrx_bot_filter_trailing",0x0124,0x14,1,0x0));
  registers.push_back(new Register("hrx_bot_filter_leading",0x0124,0x15,1,0x0));
  registers.push_back(new Register("hrx_bot_en_r",0x0124,0x16,1,0x0));
  registers.push_back(new Register("hrx_bot_en",0x0124,0x17,1,0x1));
  registers.push_back(new Register("tx_strength",0x0124,0x18,2,0x3));
  registers.push_back(new Register("dll_bias_val",0x0128,0x0,7,0x02));
  registers.push_back(new Register("dll_bias_cal",0x0128,0x7,5,0x00));
  registers.push_back(new Register("dll_cp_comp",0x0128,0xC,3,0x0));
  registers.push_back(new Register("dll_ctrlval",0x0128,0xF,4,0x0));
  registers.push_back(new Register("dll_fixctrl",0x0128,0x13,1,0x0));
  registers.push_back(new Register("dll_extctrl",0x0128,0x14,1,0x0));
  registers.push_back(new Register("dll_cp_comp_dir",0x0128,0x15,1,0x0));
  registers.push_back(new Register("dll_en_bias_cal",0x0128,0x16,1,0x0));
  registers.push_back(new Register("tg_bot_nen_fine",0x0128,0x18,1,0x1));
  registers.push_back(new Register("tg_bot_nen_coarse",0x0128,0x19,1,0x1));
  registers.push_back(new Register("tg_top_nen_fine",0x0128,0x1A,1,0x0));
  registers.push_back(new Register("tg_top_nen_coarse",0x0128,0x1B,1,0x0));
  registers.push_back(new Register("tg_cal_nrst",0x0128,0x1C,1,0x0));
  registers.push_back(new Register("tg_cal_parity_in",0x0128,0x1D,1,0x0));
  registers.push_back(new Register("pll_cp_ce",0x0130 ,0x0,1,0x0));
  registers.push_back(new Register("pll_cp_dacset",0x0130 ,0x1,7,0x53));
  registers.push_back(new Register("pll_cp_irefcpset",0x0130 ,0x8,5,0x00));
  registers.push_back(new Register("pll_cp_mmcomp",0x0130 ,0xD,3,0x0));
  registers.push_back(new Register("pll_cp_mmdir",0x0130 ,0x10,1,0x0));
  registers.push_back(new Register("pll_pfd_enspfd",0x0130 ,0x11,1,0x0));
  registers.push_back(new Register("pll_resistor",0x0130 ,0x12,5,0x1E));
  registers.push_back(new Register("pll_sw_ext",0x0130 ,0x17,1,0x0));
  registers.push_back(new Register("pll_vco_dacsel",0x0130 ,0x18,1,0x0));
  registers.push_back(new Register("pll_vco_dacset",0x0130 ,0x19,7,0x00));
  registers.push_back(new Register("pll_vco_dac_ce",0x0134,0x0,1,0x0));
  registers.push_back(new Register("pll_vco_igen_start",0x0134,0x1,1,0x0));
  registers.push_back(new Register("pll_railmode_vco",0x0134,0x2,1,0x0));
  registers.push_back(new Register("pll_abuffdacset",0x0134,0x3,5,0x00));
  registers.push_back(new Register("pll_abuffce",0x0134,0x8,1,0x0));
  registers.push_back(new Register("pll_afcvcal",0x0134,0x9,4,0x6));
  registers.push_back(new Register("pll_afcstart",0x0134,0xD,1,0x0));
  registers.push_back(new Register("pll_afcrst",0x0134,0xE,1,0x0));
  registers.push_back(new Register("pll_afc_override",0x0134,0xF,1,0x0));
  registers.push_back(new Register("pll_bt0",0x0134,0x10,1,0x0));
  registers.push_back(new Register("pll_afc_overrideval",0x0134,0x11,4,0x0));
  registers.push_back(new Register("pll_afc_overridesig",0x0134,0x15,1,0x0));
  registers.push_back(new Register("hit_phase",0x0138,0x0,2,0x1));
  registers.push_back(new Register("trig_phase",0x0138,0x4,3,0x0));
  registers.push_back(new Register("bx_rst_phase",0x0138,0x8,3,0x0));
  registers.push_back(new Register("eid_rst_phase",0x0138,0xC,3,0x0));
  registers.push_back(new Register("par_speed",0x0138,0x10,2,0x01));
  registers.push_back(new Register("par_phase",0x0138,0x14,3,0x1));
  registers.push_back(new Register("sync_clock",0x0138,0x17,1,0x1));
  registers.push_back(new Register("ext_clk_dll",0x0138,0x18,1,0x0));
  registers.push_back(new Register("shift_clk1G28",0x013C,0x4,4,0x2));
  registers.push_back(new Register("shift_clk640M",0x013C,0x8,4,0x0));
  registers.push_back(new Register("shift_clk320M",0x013C,0xC,4,0x0));
  registers.push_back(new Register("shift_clk320M_ref",0x013C,0x10,4,0x0));
  registers.push_back(new Register("shift_clk160M",0x013C,0x14,4,0x0));
  registers.push_back(new Register("shift_clk80M",0x013C,0x18,4,0x0));
  registers.push_back(new Register("shift_clk40M",0x013C,0x1C,4,0x0));
  registers.push_back(new Register("tm_top_en_falling",0x0140,0x0,0x20,0x00000000));
  registers.push_back(new Register("tm_bot_en_falling",0x0144,0x0,0x20,0x00000000));
  registers.push_back(new Register("tm_top_en_highres",0x0148,0x0,0x20,0xFFFFFFFF));
  registers.push_back(new Register("tm_bot_en_highres",0x014C,0x0,0x20,0xFFFFFFFF));
  registers.push_back(new Register("tm_top_en_scan",0x0150,0x0,0x20,0x00000000));
  registers.push_back(new Register("tm_bot_en_scan",0x0154,0x0,0x20,0x00000000));
  registers.push_back(new Register("tm_top_scan_in",0x0158,0x0,0x20,0x00000000));
  registers.push_back(new Register("tm_bot_scan_in",0x015C,0x0,0x20,0x00000000));

  registers.push_back(new Register("missing_1dc",0x012C,0x1F,32,0x0));
  
  registers.push_back(new Register("something",0x0020,0x0,0x20,0x00000000));

  registers.push_back(new Register("enable_channel_1",0x024,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_2",0x028,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_3",0x02C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_4",0x030,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_5",0x034,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_6",0x038,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_7",0x03C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_8",0x040,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_9",0x044,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_10",0x048,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_11",0x04C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_12",0x050,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_13",0x054,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_14",0x058,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_15",0x05C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_16",0x060,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_17",0x064,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_18",0x068,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_19",0x06C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_20",0x070,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_21",0x074,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_22",0x078,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_23",0x07C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_24",0x080,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_25",0x084,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_26",0x088,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_27",0x08C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_28",0x090,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_29",0x094,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_30",0x098,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_31",0x09C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_32",0x0A0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_33",0x0A4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_34",0x0A8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_35",0x0AC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_36",0x0B0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_37",0x0B4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_38",0x0B8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_39",0x0BC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_40",0x0C0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_41",0x0C4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_42",0x0C8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_43",0x0CC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_44",0x0D0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_45",0x0D4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_46",0x0D8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_47",0x0DC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_48",0x0E0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_49",0x0E4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_50",0x0E8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_51",0x0EC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_52",0x0F0,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_53",0x0F4,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_54",0x0F8,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_55",0x0FC,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_56",0x100,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_57",0x104,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_58",0x108,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_59",0x10C,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_60",0x110,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_61",0x114,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_62",0x118,0x1F,1,0x0));
  registers.push_back(new Register("enable_channel_63",0x11C,0x1F,1,0x0));  
  registers.push_back(new Register("enable_channel_64",0x120,0x1F,1,0x0));


//  cout << "AAA" << endl;
  for(uint32_t i=0; i<registers.size();i++){
    registers.at(i)->SetUpdated(false);
  }

}

PicoTDC::~PicoTDC(){
  if(m_verbose){cout << "PicoTDC::Destructor" << endl;}
  while(!registers.empty()){
    Register * reg = registers.back();
    registers.pop_back();
    //if(m_verbose){cout << "Delete register: " << reg->GetName() << endl;}
    delete reg;
  }
}

void PicoTDC::SetVerbose(bool enable){
  m_verbose=enable;
}

bool PicoTDC::Connect(string connstr){
  if(m_verbose){cout << "PicoTDC::Connect" << endl;}
  m_ipb_pico = new ipbus::Uhal(connstr);
  return m_ipb_pico->IsSynced();
}

void  PicoTDC::Sync(){
  m_ipb_pico->Sync();
}

void PicoTDC::Init(){
  if(m_verbose){cout << "PicoTDC::Init" << endl;}
  m_ipb_pico->Write(I2C_CTR,I2C_CTR_ENABLE);
  
  uint32_t freq=100e3;
  float ipbus_clock=31.25e6;
  uint32_t prescaler = int(ipbus_clock / (5*freq)) - 1;
  m_ipb_pico->Write(I2C_PRESCALE_LO, prescaler & 0xff);
  m_ipb_pico->Write(I2C_PRESCALE_HI,(prescaler >> 8) & 0xff);
 
}

void PicoTDC::Configure(){
  if(m_verbose){cout << "PicoTDC::Configure" << endl;}
  map<uint32_t,uint32_t> config;
  map<uint32_t,uint32_t>::iterator configIt;
  for(uint32_t i=0; i<registers.size();i++){
    if(!registers.at(i)->Updated()){continue;}
    cout << "Register: 0x" << hex << registers.at(i)->GetAddress() << dec << " updated" << endl;
    config[registers.at(i)->GetAddress()]=0;
    registers.at(i)->SetUpdated(false);
  }
  for(uint32_t i=0; i<registers.size();i++){
    configIt = config.find(registers.at(i)->GetAddress());
    if(configIt==config.end())continue;
    uint32_t val=configIt->second;
    //cout << "val: 0x" << hex << val << dec << endl; 
    //cout << "mas: 0x" << hex << registers.at(i)->GetMask() << dec<< endl;
    //cout << "new: 0x" << hex << (registers.at(i)->GetValue() << registers.at(i)->GetStartBit())<< dec<< endl;
    val&=~registers.at(i)->GetMask();
    //cout << "amk: 0x" << hex << val << dec << endl; 
    val|=(registers.at(i)->GetValue()<<registers.at(i)->GetStartBit());
    //cout << "val: 0x" << hex << val << dec << endl; 
    //cout << endl;
    config[registers.at(i)->GetAddress()]=val;
  } 
  for(map<uint32_t,uint32_t>::iterator it=config.begin();it!=config.end();it++){
    uint32_t adr=it->first;
    uint32_t val=it->second;
    uint32_t rda=(((adr&0xFF)<<8)&0xFF00) | ((adr>>8)&0xFF);
    uint32_t wr1=(((val<<16) & 0xFFFF0000) | rda);
    uint32_t wr2=(((val      & 0xFFFF0000) | rda | 0x0200));
    if(m_verbose){cout << "PicoTDC::Write"
		       << hex << setfill('0')
		       << " value: 0b" << setw(32) << bitset<32>(val) 
		       << " address: 0x" << setw(4) << adr 
		       << " wr1: 0x" << setw(8) << wr1
		       << " wr2: 0x" << setw(8) << wr2
		       << dec << endl;
    }
//    uint32_t wr1e = (((wr1)&0xFF)<<24)|(((wr1)&0xFF00)<<8)|(((wr1)&0xFF0000)>>8)|(((wr1)&0xFF000000)>>24);
 //   uint32_t wr2e = (((wr2)&0xFF)<<24)|(((wr2)&0xFF00)<<8)|(((wr2)&0xFF0000)>>8)|(((wr2)&0xFF000000)>>24);
 //   cout<< "wr1: 0x" << hex << wr1 << dec << endl;
 //   cout<< "wr1e: 0x" << hex << wr1e << dec << endl;
 //   cout<< "wr2: 0x" << hex << wr2 << dec << endl;
 //   cout<< "wr2e: 0x" << hex << wr2e << dec << endl;

    m_ipb_pico->Write(I2C_DATA,wr1);
    usleep(20000);
    m_ipb_pico->Write(I2C_DATA,wr2);
    usleep(20000);
  }  
}

void PicoTDC::SetValue(string name, uint32_t value){
  if(m_verbose){cout << "PicoTDC::SetValue name: " << name << " value: 0x" << hex << value << dec << endl;}
  for(uint32_t i=0; i<registers.size();i++){
    if(registers.at(i)->GetName()==name){registers.at(i)->SetValue(value);return;}
  } 
  if(m_verbose){cout << "PicoTDC::SetValue Register not found: " << name << endl;}
}

uint32_t PicoTDC::GetValue(string name){
  if(m_verbose2){cout << "PicoTDC::GetValue name:" << name << endl;}
  for(uint32_t i=0; i<registers.size();i++){
    if(registers.at(i)->GetName()==name){return registers.at(i)->GetValue();}
  } 
  return 0;
}

void PicoTDC::Blink(uint32_t led, bool enable){
  //if(m_verbose){cout << "PicoTDC::Blink led:" << led << " enable:" << enable << endl;}
  if(led>6){return;}
  if(!m_ipb_pico){return;}
  m_ipb_pico->Write(LED2+led,enable);
}

void PicoTDC::Read(vector<uint32_t> &data){
  /**
    _IDLE = 0xD
    _HEADER_1 = 0x8
    _HEADER_2 = 0x9
    _TRAILER = 0xA
    _SEPARATOR = 0xF
    _FRAMES={
        _IDLE:      'Idle Frame:  ',
        _HEADER_1:  'Header 1:    ',
        _HEADER_2:  'Header 2:    ',
        _TRAILER:   'Trailer:     ',
        _SEPARATOR: 'Separator:   '
    }
    _FRAMESN={
        _IDLE:      '5 ',
        _HEADER_1:  '1 ',
        _HEADER_2:  '4 ',
        _TRAILER:   '3 ',
        _SEPARATOR: '0 '
    }
  **/
  if(m_verbose){cout << "PicoTDC::Read" << endl;}
  if(!m_ipb_pico){return;}
  uint32_t cnt=0;
  m_ipb_pico->SetVerbose(false);
  m_ipb_pico->Read(FIFO_STATUS,cnt);
  cout << "FIFO_STATUS: " << cnt << endl; 
  m_ipb_pico->SetVerbose(false);
  cnt&=FIFO_STATUS_CNT;
  bool killME=false;
  cout << "FIFO_STATUS AFTER: " << cnt << endl; 
  if (cnt!=0) {
	//exit(-1);
     //killME=true;
  }
  data.clear();
  data.resize(cnt);
  uint32_t pos=0;
  //cout << " 1 : " << pos << " " << cnt << " " << endl;
  while(pos<cnt){
    uint32_t nw=200;
    if(cnt-pos<nw){nw=(cnt-pos);}
    //cout << pos << " " << cnt << " " << nw << " " << endl;
    m_ipb_pico->Read(FIFO_READ,&(data.at(pos)),nw,true);
    pos+=nw;
  }
  //for(uint32_t i=0; i < data.size(); i ++)
  //  cout << i << "  " << hex << data.at(i) << dec << endl;
  if (killME) exit(-1);
}

void PicoTDC::ReadData(vector<PicoTDCData>& data){
  //if(m_verbose){cout << "PicoTDC::Read" << endl;}
  if(!m_ipb_pico){return;}
  Sync();
  uint32_t cnt=0;
  std::stringstream stream;
  m_ipb_pico->Read(FIFO_STATUS,cnt);
  stream << "0x" <<std::hex << cnt;
  cnt&=FIFO_STATUS_CNT;
  data.clear();
  m_raw.clear();
  m_raw.resize(cnt);
  uint32_t pos=0;
  while(pos<cnt){
    uint32_t nw=200;
    if(cnt-pos<nw){nw=(cnt-pos);}
    m_ipb_pico->Read(FIFO_READ,&(m_raw.at(pos)),nw,true);
    pos+=nw;
  }
  for(uint32_t i=0;i<cnt;i++){
   /* 
    cout << " Valerio: " << m_raw.at(i) << endl;
    int v=m_raw.at(i);
    if ( v>>31 ) {
      if ( ((v & 0xF0000000)>>28) == 0x8) {
	cout << " 1 : " << ((v&0X07FFC000)>>15) << " : " << ((v&0X0F000000)>>26) << " : " << (v&0X3FFFF) << endl;
      }
      if ( ((v & 0xF0000000)>>28) == 0x9) {
	cout << " 4 : " << " something!!!!" << endl;
      }
      if ( ((v & 0xF0000000)>>28) == 0xA) {
	cout << " 3 : " << ((v&0X07FFC000)>>15) << " : " << ((v&0X0F000000)>>26) << " : " << ((v&0X00007FFC)>>2) << " : " << (v&0X3) << endl;
      }
      if ( ((v & 0xF0000000)>>28) == 0xF) {
	cout << " 0 : " << ((v&0x0C000000)>>26) << " : " << ((v&0x0F000000)>>26) << " : " << (v&0xFFFFFF) << endl;  
      }
    } else {
      cout << " 2 : " << ((v&0x78000000)>>27) << " : " << ((v&0x7FFF800)>>11) << " : " << ((v&0x7FF)) << endl;
    }
    */
    m_decoder.Decode(m_raw.at(i));
    if(m_decoder.IsValid()){
      data.push_back(m_decoder.Copy());
    }
  }
  //if (stream.str() == "0x10000") Sync(); 
  //if (stream.str() != "0x10000") cout << stream.str() << endl;; 
}


void PicoTDC::Startup(){
  SetValue("magic_word",0);

  for(int i=1; i < 65; i++)
  {
    stringstream test;
    test << "enable_channel_" << i;
    cout << test.str() << endl;
    SetValue(test.str(),0);
    //Configure();
    usleep(20000);
  }
  //Default config
  for(uint32_t i=0; i<registers.size();i++){
    registers.at(i)->SetUpdated(true);
  }
  Configure();

  SetValue("enable_channel_1",1);
  Configure();
  
  //Initialize PLL with AFC
  SetValue("pll_afcrst",1);
  Configure();
  usleep(100000);
  SetValue("pll_afcrst",0);
  SetValue("pll_afcstart",1);
  Configure();
  usleep(100000);
  SetValue("pll_afcstart",0);
  Configure();
  usleep(100000);
  
  //Initialize DLL
  SetValue("dll_fixctrl",1);
  Configure();
  usleep(100000);
  
  SetValue("dll_fixctrl",0);
  Configure();
  usleep(100000);
   
  //Turn everything on
  SetValue("magic_word",0x5C);
  Configure();
  usleep(100000);
//for(uint32_t i=0; i<registers.size();i++){
//    cout << registers.at(i)->GetName() << "  " <<  hex << registers.at(i)->GetAddress() <<" "  <<  registers.at(i)->GetValue()  << "  " <<  setw(32) << bitset<32>(registers.at(i)->GetValue()) << dec<< endl;
 // }
 // for(uint32_t i=0; i<registers.size();i++){
 //   registers.at(i)->SetUpdated(true);
 // }
  //Configure();

}

void PicoTDC::SetTotModeTop(){
  //set falling edge mode top
  SetValue("tm_top_en_falling",0xFFFFFFFF);
  SetValue("falling_en",1);  
  Configure();
  usleep(100000);

  //Set TDC in fine mode
  SetValue("tg_top_nen_fine",0);
  SetValue("tg_top_nen_coarse",0);
  SetValue("highres_en",1);  
  SetValue("tm_top_en_highres",0xFFFFFFFF);
  Configure();	
  usleep(100000);

  //top en scan
  SetValue("tm_top_en_scan",0xFFFFFFFF);
  SetValue("tm_top_scan_in",0x0);
  Configure();	
  usleep(100000);
  
  usleep(300000);
  
  //Set coarse top
  SetValue("tg_top_nen_fine",1);	
  SetValue("tg_top_nen_coarse",0);
  SetValue("highres_en",0);
  SetValue("tm_top_en_highres",0x00000000);
  Configure();
  usleep(100000);
        
  //top en scan
  SetValue("tm_top_en_scan",0x0);
  Configure();
  usleep(100000);

  //Set tot mode
  SetValue("tot",1);
  Configure();
  usleep(100000);
}

void PicoTDC::SetTotModeBot(){
  //set falling edge mode top
  SetValue("tm_bot_en_falling",0xFFFFFFFF);
  SetValue("falling_en",1);  
  Configure();
  usleep(100000);
  
  //Set fine bot
  SetValue("tg_bot_nen_fine",0);
  SetValue("tg_bot_nen_coarse",0);
  SetValue("highres_en",1);
  SetValue("tm_bot_en_highres",0xFFFFFFFF);

  Configure();	
  usleep(100000);

  //bot en scan
  SetValue("tm_bot_en_scan",0xFFFFFFFF);
  SetValue("tm_bot_scan_in",0x0);
  Configure();	
  usleep(100000);
  
  usleep(300000);
  
  //Set coarse bot
  SetValue("tg_bot_nen_fine",1);	
  SetValue("tg_bot_nen_coarse",0);
  SetValue("highres_en",0);
  SetValue("tm_bot_en_highres",0x00000000);
  Configure();
  usleep(100000);
        
  //bot en scan
  SetValue("tm_bot_en_scan",0x0);
  Configure();
  usleep(100000);

  //Set tot mode
  SetValue("tot",1);
  Configure();  
}

void PicoTDC::EnableChannel(uint32_t chn, bool enable){
  if(m_verbose){cout << "PicoTDC::EnableChannel channel: " << chn << endl;}
  SetValue("single_readout_en",1);
  ostringstream schn;
  schn << "enable_channel_" << chn;
  if(enable) SetValue(schn.str(),1);
  else SetValue(schn.str(),0);
  Configure();  
  usleep(100000);
}
  
void PicoTDC::EnableTrigger(bool enable){
  if(enable){
    SetValue("untriggered",0);
    SetValue("dig_rst_ext_en",1);
    SetValue("header_fields0",2);
    SetValue("header_fields1",3);
    usleep(100000);

  }else{
  Configure();
    SetValue("untriggered",1);
    SetValue("dig_rst_ext_en",0);
  }
  Configure();
  usleep(100000);
}

void PicoTDC::SetTriggerLatency(uint32_t latency){
  SetValue("trigger_latency_low",latency);
  Configure();
}

void PicoTDC::SetTriggerWindow(uint32_t window){
  SetValue("trigger_window",window);
  Configure();
}


void PicoTDC::configFromFile(std::string file ="default.txt"){

  std::string line;
  ifstream myfile ("configs/"+file);

  std::string delimiter = " ";

  if (myfile.is_open())
  {
    while ( getline (myfile,line) )
    {
      size_t pos = 0;
      std::string token;
      while ((pos = line.find(delimiter)) != std::string::npos) {
        token = line.substr(0, pos);
        line.erase(0, pos + delimiter.length());
        if (token.find("trigger_latency_low") != std::string::npos) SetTriggerLatency(std::stoi(line));
        else if (token.find("trigger_window") != std::string::npos) SetTriggerWindow(std::stoi(line));
        else if (token.find("tot_startbit") != std::string::npos) {SetValue("tot_startbit", std::stoi(line)); EvalScaleFactor(std::stoi(line)) ; Configure(); }
        else {cout << "Unkown command for pico in config file -" +token+"-, supported commands: trigger_latency_low x, trigger_window y" << endl; exit(0);}
        usleep(1000000);
      }
    }
    myfile.close();
  }

  else {cout << "Unable to open pico config file, exit(0)!" << endl; exit(0);}

}

float PicoTDC::GetScaleFactor(){ 
  return m_scale_factor_tot;
}

void PicoTDC::EvalScaleFactor(int val){

  if (val ==0) m_scale_factor_tot =3.0;
  else if (val ==1) m_scale_factor_tot =6.1;
  else if (val ==2) m_scale_factor_tot =12.2;
  else if (val ==3) m_scale_factor_tot =24.4;
  else if (val ==4) m_scale_factor_tot =49.0;
  else {
    cout << "tot_startbit value wrong, available options are 0,1,2,3,4 check config file" << endl;
    exit(0);
    return ;
  }

}


void PicoTDC::Reset(){
  //Reset TDC 
  SetValue("digital_reset",1);
  Configure();
  usleep(20000);
  SetValue("digital_reset",1);
  Configure();
  usleep(20000);
  SetValue("digital_reset",1);
  Configure();
  usleep(20000);
  //Emtpy FIFO
  vector<uint32_t> data;
  Read(data);
  //Reset TDC 
  SetValue("digital_reset",0);
  Configure();  
  usleep(20000);
}

void PicoTDC::ResetCounters(){

  SetValue("eventid_reset",1);
  usleep(20000);
  Configure();
  SetValue("eventid_reset",1);
  usleep(20000);
  Configure(); 
  SetValue("eventid_reset",1);
  usleep(20000);
  Configure();
  //Emtpy FIFO
  vector<uint32_t> data;
  Read(data);
 
  SetValue("eventid_reset",0);
  usleep(20000);
  Configure();
  

}
