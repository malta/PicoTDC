#include "PicoTDC/PicoTDCData.h"
#include <bitset>
#include <iostream>
#include <iomanip>

using namespace std;

PicoTDCData::PicoTDCData(){
  m_mode=MODE_TOT_0;
  m_eventid=0;
  m_valid=false;
  m_leading=0;
  m_channel=0;
  m_tot=0;
}

PicoTDCData::~PicoTDCData(){}

void PicoTDCData::Decode(uint32_t data){
  /*
  if (((data>>ISDATA_BIT)&ISDATA_MASK)==ISDATA) {cout << data << endl;

  if((data>>CHANNEL_BIT)&CHANNEL_MASK){ cout << ( (data>>CHANNEL_BIT)&CHANNEL_MASK ) << endl;}
  }
  */
  if(((data>>ISDATA_BIT)&ISDATA_MASK)!=ISDATA){
    m_valid=false;
    if(((data>>HEADER_BIT)&HEADER_MASK)==HEADER_1){
      m_eventid=(EVTID_MASK&(data>>EVTID_BIT));
    }
  }
  else{
    m_valid=true;
    m_channel=(data>>CHANNEL_BIT)&CHANNEL_MASK;
    if(m_mode==MODE_TOT_1){
      m_leading=(data>>LEADING_1_BIT)&LEADING_1_MASK;
      m_tot=(data>>TOT_1_BIT)&TOT_1_MASK;
    }else if(m_mode==MODE_TOT_0){
      m_leading=(data>>LEADING_0_BIT)&LEADING_0_MASK;
      m_tot=(data>>TOT_0_BIT)&TOT_0_MASK;
    }
    if(false){
      cout << hex  
	   << "data: 0x" << data << endl
	   << "chn:  0x" << m_channel << endl
	   << "lead: 0x" << m_leading << endl
	   << "tot:  0x" << m_tot << endl
	   << "evid: 0x" << m_eventid << endl
	   << dec << endl;
    }
  }


   //if (data >> 31){
   // if (((data & 0xF0000000)>>28) == 0x8){ // #header is 1000
   //    cout << (data>>28 )+" : "+ ((data & 0X07FFC000)>>15) << endl;} 
  //}
}

PicoTDCData PicoTDCData::Copy(){
  PicoTDCData copy;
  copy.m_mode=m_mode;
  copy.m_eventid=m_eventid;
  copy.m_channel=m_channel;
  copy.m_leading=m_leading;
  copy.m_tot=m_tot;
  copy.m_valid=m_valid;
  return copy;
}

void PicoTDCData::SetMode(uint32_t mode){
  m_mode=mode;
}

void PicoTDCData::SetEventId(uint32_t eventid){
  m_eventid=eventid;
}

void PicoTDCData::SetChannel(uint32_t channel){
  m_channel=channel;
}

void PicoTDCData::SetLeading(uint32_t leading){
  m_leading=leading;
}

void PicoTDCData::SetTot(uint32_t tot){
  m_tot=tot;
}

uint32_t PicoTDCData::GetEventId(){
  return m_eventid;
}

uint32_t PicoTDCData::GetLeading(){
  return m_leading;
}

uint32_t PicoTDCData::GetTot(){
  return m_tot;
}

uint32_t PicoTDCData::GetChannel(){
  return m_channel;
}

uint32_t PicoTDCData::GetMode(){
  return m_mode;
}

bool PicoTDCData::IsValid(){
  return m_valid;
}

