#include "PicoTDC/PicoTDCAnalysis.h"
#include <iostream>

using namespace std;

PicoTDCAnalysis::PicoTDCAnalysis(string run){

  m_run_number = run;
  SetupAnalysis();

}

PicoTDCAnalysis::~PicoTDCAnalysis(){
  if(m_file_pico) delete m_file_pico;
}

void PicoTDCAnalysis::SetupAnalysis(){
  m_file_pico = TFile::Open(("./blahblah/run_run_"+m_run_number+".root.root").c_str(),"read");
  m_tree_pico =  (TTree*) m_file_pico->Get("PICOTDC");


  TH1D* h_tmp_LE=NULL;
  TH1D* h_tmp_LE_nojitter=NULL;
  TH1D* h_tmp_TOT=NULL;
  TH1D* h_tmp_Nhits=NULL;

  vector<string> h_title = {"ch0","ch1","ch3","ch15"};

  double binminLE =0.; 
  double binmaxLE=200.;
  int    nbinsLE=200;

  double binminTOT =-0.2; 
  double binmaxTOT=10.;
  int    nbinsTOT= (binmaxTOT-binminTOT)/ 0.0244; // why 0.0244??


  for (unsigned int i =0; i < 4; ++i){      //4 histograms 4 channels

    h_tmp_LE = new TH1D ( (h_title.at(i)+"_le").c_str(), (h_title.at(i)+"_le").c_str(), nbinsLE, binminLE , binmaxLE);
    h_tmp_LE_nojitter = new TH1D ( (h_title.at(i)+"_le_nojit").c_str(), (h_title.at(i)+"_le_nojit").c_str(), nbinsLE, binminLE , nbinsLE);
    h_tmp_TOT = new TH1D ( (h_title.at(i)+"_TOT").c_str(), (h_title.at(i)+"_TOT").c_str(), nbinsTOT, binminTOT , binmaxTOT);
    h_tmp_Nhits = new TH1D ( (h_title.at(i)+"_Nhits").c_str(), (h_title.at(i)+"_Nhits").c_str(), 5, -0.5 , 4.5);

    m_hist_LE.push_back(h_tmp_LE);
    m_hist_LE_nojitter.push_back(h_tmp_LE_nojitter);
    m_hist_TOT.push_back(h_tmp_TOT);
    m_hist_Nhits.push_back(h_tmp_Nhits);

  }

  float minV=-20;
  float maxV= 20.0;
  float nBins=(maxV-minV)/0.4;
  m_hist01 = new TH1D("dist_p0_p1","dist_p_0_p_1; dist p_0 p_1 [ns]",int(nBins),minV,maxV);
  m_hist02 = new TH1D("dist_p0_p2","dist_p_0_p_2; dist p_0 p_2 [ns]",int(nBins),minV,maxV);
  m_hist03 = new TH1D("dist_p0_p3","dist_p_0_p_3; dist_p_0 p_3 [ns]",int(nBins),minV,maxV);
  m_hist12 = new TH1D("dist_p1_p2","dist_p_1_p_3; dist p_1 p_2 [ns]",int(nBins),minV,maxV);
  m_hist13 = new TH1D("dist_p1_p3","dist_p_1_p_3; dist p_1 p_3 [ns]",int(nBins),minV,maxV);
  m_hist23 = new TH1D("dist_p2_p3","dist_p_2_p_3; dist p_2 p_3 [ns]",int(nBins),minV,maxV);

}



void PicoTDCAnalysis::Run(){

  FillHistograms();
  //FitForResults();
  SaveResults();

}


void PicoTDCAnalysis::FillHistograms(){

  UInt_t event_id;
  UInt_t event_id_corrected;
  std::vector<float> *channel0_leading;
  std::vector<float> *channel0_tot;
  std::vector<float> *channel1_leading;
  std::vector<float> *channel1_tot;
  std::vector<float> *channel3_leading;
  std::vector<float> *channel3_tot;
  std::vector<float> *channel15_leading;
  std::vector<float> *channel15_tot;
  UInt_t mode;

  channel0_leading= 0;
  channel0_tot= 0;
  channel1_leading= 0;
  channel1_tot= 0;
  channel3_leading= 0;
  channel3_tot= 0;
  channel15_leading= 0;
  channel15_tot= 0;

  TBranch *b_channel0_leading;
  TBranch *b_channel0_tot;
  TBranch *b_channel1_leading;
  TBranch *b_channel1_tot;
  TBranch *b_channel3_leading;
  TBranch *b_channel3_tot;
  TBranch *b_channel15_leading;
  TBranch *b_channel15_tot;

  m_tree_pico->SetBranchAddress("eventid",&event_id);
  m_tree_pico->SetBranchAddress("eventid_corrected",&event_id_corrected);
  m_tree_pico->SetBranchAddress("mode",&mode);
  m_tree_pico->SetBranchAddress("channel0_leading",&channel0_leading, &b_channel0_leading);
  m_tree_pico->SetBranchAddress("channel0_tot",&channel0_tot, &b_channel0_tot);
  m_tree_pico->SetBranchAddress("channel1_leading",&channel1_leading, &b_channel1_leading);
  m_tree_pico->SetBranchAddress("channel1_tot",&channel1_tot, &b_channel1_tot);
  m_tree_pico->SetBranchAddress("channel3_leading",&channel3_leading, &b_channel3_leading);
  m_tree_pico->SetBranchAddress("channel3_tot",&channel3_tot, &b_channel3_tot);
  m_tree_pico->SetBranchAddress("channel15_leading",&channel15_leading, &b_channel15_leading);
  m_tree_pico->SetBranchAddress("channel15_tot",&channel15_tot, &b_channel15_tot);

  Long64_t nentries = m_tree_pico->GetEntries();

  float jittercorrection= 0;

  for(int i=0; i<nentries; i++){
    m_tree_pico->GetEntry(i);

    if (channel15_leading->size()> 1) {std::cout << "WARNING: more than 1 trigger" << std::endl; continue;} 
    else if (channel15_leading->size()==0) {std::cout << "WARNING: less than 1 trigger" << std::endl; continue;}
    else {
      jittercorrection = channel15_leading->at(0)-140;

    } 


    FillHistogramsPerChannel(m_hist_LE.at(0), channel0_leading,  m_hist_TOT.at(0), channel0_tot,  m_hist_Nhits.at(0), m_hist_LE_nojitter.at(0), jittercorrection);
    FillHistogramsPerChannel(m_hist_LE.at(1), channel1_leading,  m_hist_TOT.at(1), channel1_tot,  m_hist_Nhits.at(1), m_hist_LE_nojitter.at(1), jittercorrection);
    FillHistogramsPerChannel(m_hist_LE.at(2), channel3_leading,  m_hist_TOT.at(2), channel3_tot,  m_hist_Nhits.at(2), m_hist_LE_nojitter.at(2), jittercorrection);
    FillHistogramsPerChannel(m_hist_LE.at(3), channel15_leading, m_hist_TOT.at(3), channel15_tot, m_hist_Nhits.at(3), m_hist_LE_nojitter.at(3), jittercorrection);

    if ( (channel0_leading->size()>0) & (channel1_leading->size()>0))  m_hist01->Fill(channel0_leading->at(0)-channel1_leading->at(0));
    if ( (channel0_leading->size()>0) & (channel3_leading->size()>0))  m_hist02->Fill(channel0_leading->at(0)-channel3_leading->at(0));
    if ( (channel0_leading->size()>0) & (channel15_leading->size()>0))  m_hist03->Fill(channel0_leading->at(0)-channel15_leading->at(0));

    if ( (channel1_leading->size()>0) & (channel3_leading->size()>0))  m_hist12->Fill(channel1_leading->at(0)-channel3_leading->at(0));
    if ( (channel1_leading->size()>0) & (channel15_leading->size()>0))  m_hist13->Fill(channel1_leading->at(0)-channel15_leading->at(0));

    if ( (channel3_leading->size()>0) & (channel15_leading->size()>0))  m_hist23->Fill(channel3_leading->at(0)-channel15_leading->at(0));


//t1->Print? t1->GentEntry(i)->? t1->GetBranch("px")->?
  }

}


void PicoTDCAnalysis::FillHistogramsPerChannel(TH1D* hlead, std::vector<float> *vlead, TH1D* htot, std::vector<float> *vtot, TH1D* hnhits, TH1D* hjitter, float jitter){

  for (unsigned int i =0 ; i < vlead->size(); ++i) hlead->Fill(vlead->at(i));
  for (unsigned int i =0 ; i < vlead->size(); ++i) hjitter->Fill(vlead->at(i) -jitter);
  for (unsigned int i =0 ; i < vtot->size(); ++i)  htot->Fill(vtot->at(i));
  hnhits->Fill(vlead->size());

} 


void PicoTDCAnalysis::FitForResults(){

}

void PicoTDCAnalysis::SaveResults(){

  TFile *f = TFile::Open(("blahblah/histo"+m_run_number+".root").c_str(),"RECREATE");
  f->cd();
  for (unsigned int i =0; i < m_hist_LE.size(); ++i){
    m_hist_LE.at(i)->Write();
    m_hist_LE_nojitter.at(i)->Write();
    m_hist_TOT.at(i)->Write();
    m_hist_Nhits.at(i)->Write();

  }
  m_hist01->Write();
  m_hist02->Write();
  m_hist03->Write();
  m_hist12->Write();
  m_hist13->Write();
  m_hist23->Write();


  f->Close();
}

 
