#include "PicoTDC/PicoTDCTree.h"
#include <iostream>
#include <fstream>
using namespace std;

PicoTDCTree::PicoTDCTree(){
  m_run=0;
  m_eventid=0;
  m_channel=0;
  m_leading=0;
  m_tot=0;
  m_mode=0;
  m_entry=0;
  m_file=0;
}

PicoTDCTree::~PicoTDCTree(){
  if(m_file) delete m_file;
}

void PicoTDCTree::Open(string filename, string options){
  m_file = TFile::Open(filename.c_str(),options.c_str());
  m_tree = new TTree("PICOTDC","DataStream");
  m_tree->Branch("eventid",&m_eventid_current,"eventid/i");
  m_tree->Branch("eventid_corrected",&m_eventid_corrected,"eventid_corrected/i");
  //m_tree->Branch("channel",&m_channel,"channel/i");
  //m_tree->Branch("leading",&m_leading,"leading/i");
  //m_tree->Branch("tot",&m_tot,"tot/i");
  m_tree->Branch("mode",&m_mode,"mode/i");


  m_tree->Branch("channel0_leading", &m_channel0_leading);
  m_tree->Branch("channel0_tot", &m_channel0_tot);
  m_tree->Branch("channel1_leading", &m_channel1_leading);
  m_tree->Branch("channel1_tot", &m_channel1_tot);
  m_tree->Branch("channel3_leading", &m_channel3_leading);
  m_tree->Branch("channel3_tot", &m_channel3_tot);
  m_tree->Branch("channel15_leading", &m_channel15_leading);
  m_tree->Branch("channel15_tot", &m_channel15_tot);
  m_tree->Branch("resetL1id", &m_resetL1id);

  m_t0 = chrono::steady_clock::now();
  m_entry = 0;
  SetupHistograms();
}

void PicoTDCTree::Set(PicoTDCData  data, PicoTDCTree* ntuple){
  uint32_t m_eventid_tmp = data.GetEventId();
  m_channel = data.GetChannel();
  m_leading = /*m_scale_factor*/ 3.0*data.GetLeading()/1000.;
  m_mode = data.GetMode();
  m_tot = m_scale_factor*data.GetTot()/1000.;
  m_timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
  m_resetL1id = 0;
  //cout << "channel: "<< m_channel << " , m_leading:  "<< m_leading << endl;
  if (m_eventid_tmp!=m_eventid_previous){      // Fill ntuples and reset vectors if new event ID is coming
    //int resetCounterCorrection =0;
    if (m_eventid_tmp==0 and m_resetFromSocket==false) m_maxforpico++;
    else if (m_resetFromSocket==true){
///////////////////////
    //string filename("tmp.txt");
    //std::ofstream file_out;

    //file_out.open(filename, std::ios_base::app);
    //file_out <<  " reset called " <<to_string(m_eventid_tmp)  << endl;
/////////////////////
      m_resetL1id=1;
      int tmp = m_eventid_previous;
      if (tmp <= 4096) resetCounterCorrection += 4096; //abs(4096- tmp) +tmp ;
      else  resetCounterCorrection += 4096- abs(4096- tmp) +tmp ;
      m_resetFromSocket=false;
 
     //file_out << to_string(resetCounterCorrection) << " " << to_string(abs(4096- tmp) +tmp) << " " << to_string(tmp) << endl;
     //file_out.close();
    }
    m_eventid_previous = m_eventid_tmp; 
    m_eventid_current = m_eventid_tmp; 
    m_eventid_corrected = m_eventid_tmp + (8192*m_maxforpico) + resetCounterCorrection;

    ntuple->Fill();
  }

  if  (m_channel ==0) pushvectors(&m_channel0_leading, m_leading, &m_channel0_tot, m_tot);
  if  (m_channel ==1) pushvectors(&m_channel1_leading, m_leading, &m_channel1_tot, m_tot);
  //if  (m_channel ==3) pushvectors(&m_channel3_leading, m_leading, &m_channel3_tot, m_tot);
  //if  (m_channel ==15) pushvectors(&m_channel15_leading, m_leading, &m_channel15_tot, m_tot);
  if  (m_channel ==15) pushvectors(&m_channel3_leading, m_leading, &m_channel3_tot, m_tot);
  if  (m_channel ==3) pushvectors(&m_channel15_leading, m_leading, &m_channel15_tot, m_tot);
}


void  PicoTDCTree::pushvectors(std::vector<float> *vlead, float lead, std::vector<float> *vtot, float tot){

  vlead->push_back(lead);
  vtot->push_back(tot);

}



void PicoTDCTree::SetRunNumber(uint32_t run){
  m_run=run;
}

void PicoTDCTree::Fill(){
  //std::cout<< "Event " << m_eventid_current << " recorded" <<std::endl;
  //std::cout << m_channel0_leading.size() << " " << m_channel1_leading.size()  << " " << m_channel3_leading.size() << " "  << m_channel15_leading.size() << std::endl;

  FillHistograms();
  m_tree->Fill();
  m_channel0_leading.clear();
  m_channel0_tot.clear();
  m_channel1_leading.clear();
  m_channel1_tot.clear();
  m_channel3_leading.clear();
  m_channel3_tot.clear();
  m_channel15_leading.clear();
  m_channel15_tot.clear();
  
}

int PicoTDCTree::Next(){
  int ret=m_tree->GetEntry(m_entry);
  m_entry++;
  return ret;
}

PicoTDCData * PicoTDCTree::Get(){
  m_data.SetEventId(m_eventid);
  m_data.SetChannel(m_channel);
  m_data.SetLeading(m_leading);
  m_data.SetMode(m_mode);
  m_data.SetTot(m_tot);
  return &m_data;
}

float PicoTDCTree::GetTimer(){
  return m_timer;
}

uint32_t PicoTDCTree::GetRunNumber(){
  return m_run;
}

TFile* PicoTDCTree::GetFile(){
  return m_file;
}

void PicoTDCTree::Close(){
  m_file->cd();
  m_tree->Write();
  m_file->Close();
}
 
void PicoTDCTree::SetScaleFactorTOT(float val){
  m_scale_factor=val;
  return;
}


 
void PicoTDCTree::SetupHistograms(){

  // 12 histograms 3 channel0, 3 channel1, 3 channel 3, 3 channel15 (trigger)
  TH1D* h_tmp_LE=NULL;
  TH1D* h_tmp_LE_nojitter=NULL;
  TH1D* h_tmp_Nhits=NULL;

  vector<string> h_title = {"ch0","ch1","ch3","ch15"};

  double binminLE =0.; 
  double binmaxLE=500.;
  int    nbinsLE=500;
  m_vector_histograms = new std::vector<TH1D*>;
  for (unsigned int i =0; i < 4; ++i){   

    h_tmp_LE = new TH1D ( (h_title.at(i)+"_le").c_str(), (h_title.at(i)+"_le").c_str(), nbinsLE, binminLE , binmaxLE);
    h_tmp_LE_nojitter = new TH1D ( (h_title.at(i)+"_le_nojit").c_str(), (h_title.at(i)+"_le_nojit").c_str(), 2*nbinsLE, binminLE , binmaxLE);
    h_tmp_Nhits = new TH1D ( (h_title.at(i)+"_Nhits").c_str(), (h_title.at(i)+"_Nhits").c_str(), 5, -0.5 , 4.5);

    m_vector_histograms->push_back(h_tmp_Nhits);
    m_vector_histograms->push_back(h_tmp_LE);
    m_vector_histograms->push_back(h_tmp_LE_nojitter);

  }

}

void PicoTDCTree::FillHistograms(){
  if (m_channel15_leading.size()<1) {
    cout << "PicoTDCTree::FillHistograms: channel 15 empty, I cannot update the plot, check the connection" << endl;
    //return;
  }
  //channel 0
  m_vector_histograms->at(0)->Fill(m_channel0_leading.size());
  for (unsigned int i =0; i < m_channel0_leading.size(); ++i){
    m_vector_histograms->at(1)->Fill(m_channel0_leading.at(i));
    if (m_channel15_leading.size()>0) m_vector_histograms->at(2)->Fill(m_channel0_leading.at(i) - m_channel15_leading.at(0) +140);
    else m_vector_histograms->at(2)->Fill(0.5);
  }

  //channel 1
  m_vector_histograms->at(3)->Fill(m_channel1_leading.size());
  for (unsigned int i =0; i < m_channel1_leading.size(); ++i){
    m_vector_histograms->at(4)->Fill(m_channel1_leading.at(i));
    if (m_channel15_leading.size()>0) m_vector_histograms->at(5)->Fill(m_channel1_leading.at(i)- m_channel15_leading.at(0)+140);
    else m_vector_histograms->at(5)->Fill(0.5);
  }
  
  //channel 3 
  m_vector_histograms->at(6)->Fill(m_channel3_leading.size());
  for (unsigned int i =0; i < m_channel3_leading.size(); ++i){
    m_vector_histograms->at(7)->Fill(m_channel3_leading.at(i));
    if (m_channel15_leading.size()>0) m_vector_histograms->at(8)->Fill(m_channel3_leading.at(i) - m_channel15_leading.at(0)+140);
    else m_vector_histograms->at(8)->Fill(0.5);
  }

  //channel 15 trigger
  m_vector_histograms->at(9)->Fill(m_channel15_leading.size());
  for (unsigned int i =0; i < m_channel15_leading.size(); ++i){
    m_vector_histograms->at(10)->Fill(m_channel15_leading.at(i));
    if (m_channel15_leading.size()>0) m_vector_histograms->at(11)->Fill(m_channel15_leading.at(i) - m_channel15_leading.at(0)+140);
    else m_vector_histograms->at(11)->Fill(0.5);
  }




}


std::vector<TH1D*>* PicoTDCTree::RetrieveHistograms(){

  return m_vector_histograms;

}

void PicoTDCTree::ResetCounters(){ m_resetFromSocket = true; }
