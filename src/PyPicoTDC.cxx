/************************************
 * PyPicoTDC
 * Brief: Python module for PicoTDC
 * 
 * Author: Carlos.Solans@cern.ch
 ************************************/

#define PY_SSIZE_T_CLEAN //Not sure we need this
#include <Python.h>
#include <stddef.h>
#include <iostream>
#include "PicoTDC/PicoTDC.h"

#if PY_VERSION_HEX < 0x020400F0

#define Py_CLEAR(op)				\
  do {						\
    if (op) {					\
      PyObject *tmp = (PyObject *)(op);		\
      (op) = NULL;				\
      Py_DECREF(tmp);				\
    }						\
  } while (0)

#define Py_VISIT(op)				\
  do {						\
    if (op) {					\
      int vret = visit((PyObject *)(op), arg);	\
      if (vret)					\
	return vret;				\
    }						\
  } while (0)

#endif //PY_VERSION_HEX < 0x020400F0


#if PY_VERSION_HEX < 0x020500F0

typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
typedef inquiry lenfunc;
typedef intargfunc ssizeargfunc;
typedef intobjargproc ssizeobjargproc;

#endif //PY_VERSION_HEX < 0x020500F0

#ifndef PyVarObject_HEAD_INIT
#define PyVarObject_HEAD_INIT(type, size)	\
  PyObject_HEAD_INIT(type) size,
#endif


#if PY_VERSION_HEX >= 0x03000000

#define MOD_ERROR NULL
#define MOD_RETURN(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
  static struct PyModuleDef moduledef = {		\
    PyModuleDef_HEAD_INIT, name, doc,-1, methods	\
  };							\
  m = PyModule_Create(&moduledef);			\

#else

#define MOD_ERROR 
#define MOD_RETURN(val) 
#define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#define MOD_DEF(ob,name,doc,methods)			\
    ob = Py_InitModule3((char *) name, methods, doc);

#endif //PY_VERSION_HEX >= 0x03000000

/************************************
 *
 * Module declaration
 *
 ************************************/

typedef struct {
  PyObject_HEAD
  PicoTDC *obj;
  std::vector<uint32_t> data;
  std::vector<PicoTDCData> proc;
} PyPicoTDC;

static int _PyPicoTDC_init(PyPicoTDC *self)
{

    self->obj = new PicoTDC();
    return 0;
}

static void _PyPicoTDC_dealloc(PyPicoTDC *self)
{
  delete self->obj;
  self->data.clear();
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject * _PyPicoTDC_Blink(PyPicoTDC *self, PyObject *args)
{
  uint32_t led;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&led,&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : false;
  self->obj->Blink(led,enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_Connect(PyPicoTDC *self, PyObject *args)
{
  char * str;
  if(PyArg_ParseTuple(args, (char *) "s",&str)){
    self->obj->Connect(std::string(str));
  }
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_Init(PyPicoTDC *self)
{
  self->obj->Init();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_Configure(PyPicoTDC *self)
{
  self->obj->Configure();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_EnableChannel(PyPicoTDC *self, PyObject *args)
{
  uint32_t chn;
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "I|O",&chn,&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : true;
  self->obj->EnableChannel(chn,enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_EnableTrigger(PyPicoTDC *self, PyObject *args)
{
  PyObject *py_enable = NULL;
  if (!PyArg_ParseTuple(args, (char *) "|O",&py_enable)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  bool enable = py_enable? (bool) PyObject_IsTrue(py_enable) : true;
  self->obj->EnableTrigger(enable);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_GetValue(PyPicoTDC *self, PyObject *args)
{
  char * name;
  if (!PyArg_ParseTuple(args, (char *) "s",&name)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  uint32_t ret = self->obj->GetValue(std::string(name));
  PyObject *py_ret;
  py_ret = PyLong_FromLong(ret);
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyPicoTDC_Read(PyPicoTDC *self)
{
  self->obj->Read(self->data);
  PyObject *py_ret;
  
  py_ret = PyList_New(self->data.size()); 
  for(uint32_t i=0;i<self->data.size();i++){
    PyObject * it = PyLong_FromLong(self->data.at(i));
    if(PyList_SetItem(py_ret,i,it)!=0){
      // Error parsing item
      Py_INCREF(Py_None);
      return(Py_None);
    }
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyPicoTDC_ReadData(PyPicoTDC *self)
{
  self->obj->ReadData(self->proc);
  PyObject *py_ret;
  
  py_ret = PyList_New(self->proc.size()); 
  for(uint32_t i=0;i<self->proc.size();i++){
    PyObject * it = PyDict_New();
    PyDict_SetItemString(it, "channel", PyLong_FromLong(self->proc.at(i).GetChannel()));
    PyDict_SetItemString(it, "eventid", PyLong_FromLong(self->proc.at(i).GetEventId()));
    PyDict_SetItemString(it, "leading", PyLong_FromLong(self->proc.at(i).GetLeading()));
    PyDict_SetItemString(it, "tot",     PyLong_FromLong(self->proc.at(i).GetTot()));
    if(PyList_SetItem(py_ret,i,it)!=0){
      // Error parsing item
      Py_INCREF(Py_None);
      return(Py_None);
    }
  }
  Py_INCREF(py_ret);
  return py_ret;
}

PyObject * _PyPicoTDC_Reset(PyPicoTDC *self)
{
  self->obj->Reset();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_SetValue(PyPicoTDC *self, PyObject *args)
{
  char * name;
  uint32_t val;
  if (!PyArg_ParseTuple(args, (char *) "sI",&name, &val)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->SetValue(std::string(name), val);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_SetTotModeTop(PyPicoTDC *self)
{
  self->obj->SetTotModeTop();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_SetTotModeBot(PyPicoTDC *self)
{
  self->obj->SetTotModeBot();
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_SetTriggerLatency(PyPicoTDC *self, PyObject *args)
{
  uint32_t val;
  if (!PyArg_ParseTuple(args, (char *) "I",&val)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->SetTriggerLatency(val);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_SetTriggerWindow(PyPicoTDC *self, PyObject *args)
{
  uint32_t val;
  if (!PyArg_ParseTuple(args, (char *) "I",&val)){
    Py_INCREF(Py_None);
    return Py_None;
  }
  self->obj->SetTriggerWindow(val);
  Py_INCREF(Py_None);
  return Py_None;
}

PyObject * _PyPicoTDC_Startup(PyPicoTDC *self)
{
  self->obj->Startup();
  Py_INCREF(Py_None);
  return Py_None;
}

static PyMethodDef PyPicoTDC_methods[] = {
  {(char *) "Blink",            (PyCFunction) _PyPicoTDC_Blink,             METH_VARARGS, NULL },
  {(char *) "Init",             (PyCFunction) _PyPicoTDC_Init,              METH_NOARGS,  NULL },
  {(char *) "Connect",          (PyCFunction) _PyPicoTDC_Connect,           METH_VARARGS, NULL },
  {(char *) "Configure",        (PyCFunction) _PyPicoTDC_Configure,         METH_NOARGS,  NULL },
  {(char *) "EnableChannel",    (PyCFunction) _PyPicoTDC_EnableChannel,     METH_VARARGS, NULL },
  {(char *) "EnableTrigger",    (PyCFunction) _PyPicoTDC_EnableTrigger,     METH_VARARGS, NULL },
  {(char *) "GetValue",         (PyCFunction) _PyPicoTDC_GetValue,          METH_VARARGS, NULL },
  {(char *) "Read",             (PyCFunction) _PyPicoTDC_Read,              METH_NOARGS,  NULL },
  {(char *) "ReadData",         (PyCFunction) _PyPicoTDC_ReadData,          METH_NOARGS,  NULL },
  {(char *) "Reset",            (PyCFunction) _PyPicoTDC_Reset,             METH_NOARGS,  NULL },
  {(char *) "SetValue",         (PyCFunction) _PyPicoTDC_SetValue,          METH_VARARGS, NULL },
  {(char *) "SetTotModeTop",    (PyCFunction) _PyPicoTDC_SetTotModeTop,     METH_VARARGS, NULL },
  {(char *) "SetTotModeBot",    (PyCFunction) _PyPicoTDC_SetTotModeBot,     METH_VARARGS, NULL },
  {(char *) "SetTriggerLatency",(PyCFunction) _PyPicoTDC_SetTriggerLatency, METH_VARARGS, NULL },
  {(char *) "SetTriggerWindow", (PyCFunction) _PyPicoTDC_SetTriggerWindow,  METH_VARARGS, NULL },
  {(char *) "Startup",          (PyCFunction) _PyPicoTDC_Startup,           METH_NOARGS,  NULL },
  {NULL, NULL, 0, NULL}
};

PyTypeObject PyPicoTDC_Type = {
    PyVarObject_HEAD_INIT(NULL, 0)
    (char *) "PyPicoTDC",                /* tp_name */
    sizeof(PyPicoTDC),                   /* tp_basicsize */
    0,                                   /* tp_itemsize */
    (destructor) _PyPicoTDC_dealloc,     /* tp_dealloc */
    (printfunc)0,                        /* tp_print */
    (getattrfunc)NULL,                   /* tp_getattr */
    (setattrfunc)NULL,                   /* tp_setattr */
    0,                                   /* tp_compare */
    (reprfunc)NULL,                      /* tp_repr */
    (PyNumberMethods*)NULL,              /* tp_as_number */
    (PySequenceMethods*)NULL,            /* tp_as_sequence */
    (PyMappingMethods*)NULL,             /* tp_as_mapping */
    (hashfunc)NULL,                      /* tp_hash */
    (ternaryfunc)NULL,                   /* tp_call */
    (reprfunc)NULL,                      /* tp_str */
    (getattrofunc)NULL,                  /* tp_getattro */
    (setattrofunc)NULL,                  /* tp_setattro */
    (PyBufferProcs*)NULL,                /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT,                  /* tp_flags */
    NULL,                                /* Documentation string */
    (traverseproc)NULL,                  /* tp_traverse */
    (inquiry)NULL,                       /* tp_clear */
    (richcmpfunc)NULL,                   /* tp_richcompare */
    0,                                   /* tp_weaklistoffset */
    (getiterfunc)NULL,                   /* tp_iter */
    (iternextfunc)NULL,                  /* tp_iternext */
    (struct PyMethodDef*)PyPicoTDC_methods, /* tp_methods */
    (struct PyMemberDef*)0,              /* tp_members */
    0,                                   /* tp_getset */
    NULL,                                /* tp_base */
    NULL,                                /* tp_dict */
    (descrgetfunc)NULL,                  /* tp_descr_get */
    (descrsetfunc)NULL,                  /* tp_descr_set */
    0,                                   /* tp_dictoffset */
    (initproc)_PyPicoTDC_init,           /* tp_init */
    (allocfunc)PyType_GenericAlloc,      /* tp_alloc */
    (newfunc)PyType_GenericNew,          /* tp_new */
    (freefunc)0,                         /* tp_free */
    (inquiry)NULL,                       /* tp_is_gc */
    NULL,                                /* tp_bases */
    NULL,                                /* tp_mro */
    NULL,                                /* tp_cache */
    NULL,                                /* tp_subclasses */
    NULL,                                /* tp_weaklist */
    (destructor) NULL                    /* tp_del */
#if PY_VERSION_HEX >= 0x02060000
	,0                               /* tp_version_tag */
#endif
};

static PyMethodDef module_methods[] = {
  {NULL, NULL, 0, NULL}
};

MOD_INIT(PyPicoTDC)
{
    PyObject *m;
    MOD_DEF(m, "PyPicoTDC", NULL, module_methods);
    if(PyType_Ready(&PyPicoTDC_Type)<0){
      return MOD_ERROR;
    }
    PyModule_AddObject(m, (char *) "PicoTDC", (PyObject *) &PyPicoTDC_Type);
    return MOD_RETURN(m);
}
