//! -*-C++-*-
#include "PicoTDC/PicoTDCTree.h"
#include "PicoTDC/PicoTDC.h"
#include "PicoTDC/PicoTDCModule.h"
#include "PicoTDC/PicoTDCAnalysis.h"
#include "PicoTDC/PicoTDCData.h"
#include <cmdl/cmdargs.h>
#include "TSysEvtHandler.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include "TCanvas.h"
#include "TH1D.h"
#include "TApplication.h"

using namespace std;
bool m_cont=true;

//bool g_cont=true;

//PicoTDCModule *m_picoModule =NULL;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  //g_cont=false;
  m_cont=false;
}

class THandler: public TSignalHandler{
public:
  THandler(ESignals sig): TSignalHandler(sig, false){}
  virtual Bool_t Notify(){
    handler(0);
    return false;
  }
};

int main(int argc, char* argv[]){


  if (argc == 1) {
    cout << "run number needed, exit(0)" << endl;
    exit(0);
  }  

  
  signal(SIGINT,handler);
  signal(SIGTERM,handler);

  THandler theHandler1(kSigInterrupt);
  THandler theHandler2(kSigTermination);
  theHandler1.Add();
  theHandler2.Add();

 
  TApplication theApp("myApp",0,0);
  TCanvas *c = new TCanvas("c","c", 1000, 750);
  c->Divide(4,3);
  
  c->Update();

  string run_number =  argv[1] ;
  PicoTDCModule *m_picoModule = new PicoTDCModule ("pico", "udp://ep-ade-gw-01:60000", "blahblah");

  m_picoModule->Configure();


  m_picoModule->Start(("run_"+run_number+".root").c_str(), false);
  sleep (5); 
  while (m_cont) {
    m_picoModule->PlotCanvasRealTime(c);    
    sleep(5);
    
  }
  m_picoModule->Stop();


  cout << "Hello World!" << endl;


  //PicoTDCAnalysis* m_analysis = new PicoTDCAnalysis(run_number);
  //m_analysis->Run();
  return 0;
}
