#ifndef PICOTDCDATA_H
#define PICOTDCDATA_H

#include <cstdint>

/**
 * PicoTDC data from the FPGA is an array of 32-bit integers
 * containing header, trailer, and payload.
 * PicoTDC::Decode decodes one 32-bit integer at a time 
 * based on the selected mode with PicoTDC::SetMode 
 * (PicoTDC::MODE_TOT_0, PicoTDC::MODE_TOT_1).
 * PicoTDC::IsValid will return true each time the decoded
 * integer is an actual data word, in which case the following
 * details of the event are available: 
 * event ID (PicoTDC::GetEventId), 
 * leading edge (PicoTDC::GetLeading),
 * TOT (PicoTDC::GetTot),
 * and channel ID (PicoTDC::GetChannel).
 *
 * @brief Container and decoder of PicoTDC data 
 * @author Carlos.Solans@cern.ch
 * @date September 2019
 **/
class PicoTDCData{

 public:
  static const uint32_t HEADER_1      = 8;
  static const uint32_t ISDATA        = 0;
  static const uint32_t MODE_TOT_0    = 0;
  static const uint32_t MODE_TOT_1    = 1;
  static const uint32_t MODE_FULL     = 2;

  static const uint32_t ISDATA_BIT    = 31; 
  static const uint32_t HEADER_BIT    = 28; 
  static const uint32_t EVTID_BIT     = 15; 
  static const uint32_t CHANNEL_BIT   = 27;
  static const uint32_t LEADING_0_BIT = 11; 
  static const uint32_t LEADING_1_BIT = 8; 
  static const uint32_t TOT_0_BIT     = 0;
  static const uint32_t TOT_1_BIT     = 0;

  static const uint32_t ISDATA_MASK   = 0x1; 
  static const uint32_t HEADER_MASK   = 0xF; 
  static const uint32_t EVTID_MASK    = 0x1FFF;
  static const uint32_t CHANNEL_MASK  = 0xF;
  static const uint32_t LEADING_0_MASK= 0xFFFF;
  static const uint32_t LEADING_1_MASK= 0x7FFFF;
  static const uint32_t TOT_0_MASK    = 0x7FF;
  static const uint32_t TOT_1_MASK    = 0xFF;

  /**
   * @brief This class holds the data from PicoTDC
   **/
  PicoTDCData();

  /**
   * @brief default destructor
   **/
  ~PicoTDCData();

  /**
   * @brief Decode a 32-bit data frame using the mode specified in the constructor. Check if the data is valid with PicoTDCData::IsValid
   * @param data a 32-bit data frame
   **/
  void Decode(uint32_t data);
 
 /**
   * @brief Copy of a PicoTDCData object
   * @return PicoTDCData object
   **/
  PicoTDCData Copy();
  
  /**
   * @brief Set the decoder mode
   * @param mode MODE_TOT_0, MODE_TOT_1, MODE_FULL
   **/
  void SetMode(uint32_t mode);

  /**
   * @brief Set the event number
   * @param eventid the event number
   **/
  void SetEventId(uint32_t eventid);

  /**
   * @brief Set the channel number
   * @param channel the channel number
   **/
  void SetChannel(uint32_t channel);

  /**
   * @brief Set the leading edge
   * @param leading the leading edge
   **/
  void SetLeading(uint32_t leading);

  /**
   * @brief Set the TOT
   * @param tot the TOT
   **/
  void SetTot(uint32_t tot);

  /**
   * @brief Get the event id
   * @return The event id
   **/
  uint32_t GetEventId();

  /**
   * @brief Get the leading edge
   * @return The leading edge
   **/
  uint32_t GetLeading();

  /**
   * @brief Get the TOT
   * @return The TOT
   **/
  uint32_t GetTot();

  /**
   * @brief Get the channel
   * @return The channel
   **/
  uint32_t GetChannel();
  
  /**
   * @brief Get the decoder mode
   * @return The decoder mode
   **/
  uint32_t GetMode();
  
  /**
   * @brief Check if the data frame is valid
   * @return the event id
   **/
  bool IsValid();

 private:

  uint32_t m_channel;
  uint32_t m_eventid;
  uint32_t m_leading;
  uint32_t m_tot;
  uint32_t m_mode;
  bool m_valid;
  

};

#endif
