#ifndef PICOTDC_H
#define PICOTDC_H

#include "ipbus/Register.h"
#include "ipbus/Uhal.h"
#include <string>
#include <vector>
#include <map>
#include <unistd.h>
#include "PicoTDC/PicoTDCData.h"

#include "PicoTDC/PicoTDCTree.h"
/**
 * This is the MaltaSW implementation of the control and read-out for Pico-TDC.
 * The methods implemented here are a copy of the methods available in the
 * legacy pico-tdc software and firmware.
 * Upon creation of a new PicoTDC object there is no connection to the hardware.
 * PicoTDC::Connect establishes the connection.
 * PicoTDC::Init initializes the I2C communication in the FPGA.
 * PicoTDC::Configure will send to the FPGA all the registers that are updated
 * with PicoTDC::SetValue. 
 * Data is available as a vector of 32-bit integers with PicoTDC::Read,
 * and as a vector of PicoTDCData objects with PicoTDC::ReadData.
 * PicoTDC::Reset will reset the PicoTDC and the FPGA.
 * 
 * The following methods are implemented for convenience:
 * * PicoTDC::Startup
 * * PicoTDC::SetTotModeTop
 * * PicoTDC::SetTotModeBot
 * * PicoTDC::EnableChannel
 * * PicoTDC::EnableTrigger
 * * PicoTDC::SetTriggerLatency
 * * PicoTDC::SetTriggerWindow
 * 
 * @brief Control and read-out for PicoTDC through IPbus 
 * @author Carlos.Solans@cern.ch
 * @date August 2019
 **/
class PicoTDC{

 public:

  static const uint32_t LED2=0x10; //LED size 0x1
  static const uint32_t LED3=0x11; //LED size 0x1
  static const uint32_t LED4=0x12; //LED size 0x1
  static const uint32_t LED5=0x13; //LED size 0x1
  static const uint32_t LED6=0x14; //LED size 0x1
  static const uint32_t LED7=0x15; //LED size 0x1
  static const uint32_t I2C_PRESCALE_LO=0x40; //FPGA I2C prescaler
  static const uint32_t I2C_PRESCALE_HI=0x41; //FPGA I2C prescaler
  static const uint32_t I2C_CTR=0x42; //FPGA I2C prescaler
  
  static const uint32_t I2C_DATA=0x30; //I2C data
  static const uint32_t I2C_CTRL=0x31; //I2C control
  static const uint32_t FIFO_STATUS=0x81; //number of words (mask 0x7FFF)
  static const uint32_t FIFO_READ=0x82; //read-out FIFO


  static const uint32_t I2C_CTR_ENABLE = 0x80;
  static const uint32_t FIFO_STATUS_CNT = 0x7FFF;
  static const uint32_t FIFO_STATUS_EMPTY = 0x10000;
  static const uint32_t FIFO_STATUS_RST = 0x20000;

  /**
   * @brief Initialize all the IPbus registers of the PicoTDC (161)
   **/
   PicoTDC();
  
  /**
   * @brief Delete the internal memory allocated
   **/
   ~PicoTDC();
  
  /**
   * @brief Connect to the hardware through IPbus
   * @param connstr Ipbus connection string: "protocol://hostname:port?target=device:port"
   * @return True if the commnunication was established
   **/
  bool Connect(std::string connstr);
  
  /**
   * @brief Initialize the I2C commnunication in the FPGA with the PicoTDC
   **/
  void Init();

  /**
   * @brief Change the state of an LED (Legacy)
   * @param led Number of the LED to enable
   * @param enable Power on the LED if true, power off if false.
   **/
  void Blink(uint32_t led, bool enable);
  
  /**
   * @brief Change the value of an IPbus Register of the PicoTDC
   * @param name Name of the IPbus Register
   * @param value New value for the Register
   **/
  void SetValue(std::string name, uint32_t value);
  
  /**
   * @brief Get the value of an IPbus Register of the PicoTDC
   * @param name Name of the IPbus Register
   * @return The value of the Register
   **/
  uint32_t GetValue(std::string name);
  
  /**
   * @brief Write the Registers that have been updated through IPBus
   **/
  void Configure();
  
  /**
   * @brief Write the Registers that have been updated through IPBus
   * @param data Vector of 32-bit integers passed by reference 
   *        to fill with the contents of the PicoTDC FIFO
   **/
  void Read(std::vector<uint32_t>& data);

  /**
   * @brief Write the Registers that have been updated through IPBus
   * @param data Vector PicoTDCData objects passed by reference 
   *        to fill with the contents of the PicoTDC FIFO
   **/
  void ReadData(std::vector<PicoTDCData>& data);

  /**
   * @brief Commodity method to Initialize the data taking with channel 1 enabled
   **/
  void Startup();

  /**
   * @brief Enable the TOT mode on the top
   **/
  void SetTotModeTop();

  /**
   * @brief Enable the TOT mode on the bottom
   **/
  void SetTotModeBot();

  /**
   * @brief Enable a given channel for read-out
   * @param chn Number of the channel to enable.
   * @param enable Enable the channel if true, disable it if false.
   **/
  void EnableChannel(uint32_t chn, bool enable=true);
  
  /**
   * @brief Enable the trigger
   * @param enable Enable the channel if true, disable it if false.
   **/
  void EnableTrigger(bool enable=true);

  /**
   * @brief Set the trigger latency
   * @param latency Number of the clock cycles to go back after trigger.
   **/
  void SetTriggerLatency(uint32_t latency);

  /**
   * @brief Set the trigger window
   * @param window Number of clock cycles for the trigger window.
   **/
  void SetTriggerWindow(uint32_t window);

  /**
   * @brief Reset the Pico-TDC and the FPGA
   **/
  void Reset();
  void Sync();
  void configFromFile(std::string file );
  /**
   * @brief Enable the verbose mode
   * @param enable Enable if true, disable if false.
   **/
  void SetVerbose(bool enable);
  void EvalScaleFactor(int val);
  float GetScaleFactor();
  void ResetCounters();
 private:
  
  std::vector<ipbus::Register*> registers;
  std::vector<uint32_t> m_raw;
  ipbus::Uhal* m_ipb_pico;
  bool m_verbose;
  bool m_verbose2;
  PicoTDCData m_decoder;
  float m_scale_factor_tot;
};

#endif
