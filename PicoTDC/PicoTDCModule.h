#ifndef PICOTDCMODULE_H
#define PICOTDCMODULE_H

#include "MaltaDAQ/ReadoutModule.h"
#include "PicoTDC/PicoTDC.h"
#include "PicoTDC/PicoTDCTree.h"

#include "TCanvas.h"
#include <string>
#include <thread>
#include <map>

#include "TString.h"

/**
 * @brief PicoTDC ReadoutModule for MaltaMultiDAQ
 * @author Carlos.Solans@cern.ch
 * @date October 2018
 **/

//bool m_cont=true;

class PicoTDCModule: public ReadoutModule{
  
 public:
  
  /**
   * @brief create a PicoTDC module
   * @param name the module name
   * @param address the ipbus connection string
   * @param outdir the output directory
   **/
  PicoTDCModule(std::string name, std::string address, std::string outdir);

  /**
   * @brief delete pointers
   **/
  ~PicoTDCModule();
  
  /**
   * @brief Set the module for internal triggering
   **/
  void SetInternalTrigger();

  /**
   * @brief Configure MALTA for data taking
   **/
  void Configure();
  
  /**
   * @brief Start data acquisition thread
   * @param run the run number as a string
   * @param usePath use the path 
   **/
  void Start(std::string run, bool usePath = false);

  /**
   * @brief Stop the data acquisition thread
   **/
  void Stop();

  /**
   * @brief The data acquisition thread
   **/
  void Run();

  /**
   * @brief get the L1A from the FPGA
   * @return L1A 
   **/
  uint32_t GetNTriggers();

  /**
   * @brief enable fast signal
   **/
  void EnableFastSignal();
  void ResetL1Counter();
  bool RetrieveL1ResetFlag();
  /**
   * @brief disable fast signal
   **/
  void DisableFastSignal();

  void Sync();
  std::vector<TH1D*>* GetRealTimeHistograms();
  void PlotCanvasRealTime(TCanvas *c);
  double findMaxBin(TH1D* h);
  double findMinBin(TH1D* h);
 private:
  
  
  PicoTDC * m_pico;
  PicoTDCTree * m_ntuple;
  bool m_internalTrigger;
  bool m_resetL1Counters=false;
};

#endif

