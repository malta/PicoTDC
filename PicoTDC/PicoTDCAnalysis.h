#ifndef PICOTDCANALYSIS_H
#define PICOTDCANALYSIS_H

#include "TFile.h"
#include "TTree.h"
#include <string>
#include "TH1F.h"
#include <vector>
#include <string>

/**
 * @brief Class to store PicoTDC data in a ROOT tree
 **/
class PicoTDCAnalysis{

 public:
  /**
   * @brief Construct and empty PicoTDCAnalysis
   **/
  PicoTDCAnalysis(std::string run);
  
  /**
   * @brief Close ROOT file if open
   **/
  ~PicoTDCAnalysis();

  void SetupAnalysis(); 
  void Run();
  void FillHistograms();
  void FitForResults();
  void SaveResults();
  void FillHistogramsPerChannel(TH1D* hlead, std::vector<float> *vlead, TH1D* htot, std::vector<float> *vtot, TH1D* hnhits, TH1D* hjitter, float jitter);


  private:
  
  TFile * m_file_pico;
  TTree * m_tree_pico;

  std::string m_run_number;
  std::vector<TH1D*> m_hist_LE;
  std::vector<TH1D*> m_hist_LE_nojitter;
  std::vector<TH1D*> m_hist_TOT;
  std::vector<TH1D*> m_hist_Nhits;


  TH1D *m_hist01;
  TH1D *m_hist02;
  TH1D *m_hist03;
  TH1D *m_hist12;
  TH1D *m_hist13;
  TH1D *m_hist23;





};

#endif

