#ifndef PICOTDCTREE_H
#define PICOTDCTREE_H

#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include <string>
#include "PicoTDC/PicoTDCData.h"
#include <chrono>

/**
 * @brief Class to store PicoTDC data in a ROOT tree
 **/
class PicoTDCTree{

 public:
  /**
   * @brief Construct and empty PicoTDCTree
   **/
  PicoTDCTree();
  
  /**
   * @brief Close ROOT file if open
   **/
  ~PicoTDCTree();
  
  /**
   * @brief Open ROOT file with given options
   * @param filename absolute path to the file
   * @param options ROOT options for new file
   **/
  void Open(std::string filename,std::string options);

  /**
   * @brief Close the ROOT file
   **/
  void Close();

  /**
   * @brief Add the current values to the Tree.
   * Set the values with PicoTDCTree::Set.
   **/
  void Fill();

  /**
   * @brief Set the values for the current event.
   * @param data The PicoTDCData containing the data.
   **/
  void Set(PicoTDCData  data, PicoTDCTree *ntuple);

  /**
   * @brief Set the run number
   * @param run the runnumber
   **/
  void SetRunNumber(uint32_t run);

  /**
   * @brief Get the run number
   * @return run the runnumber
   **/
  PicoTDCData * Get();
  
  /**
   * @brief Get the run number
   * @return the run number
   **/
  uint32_t GetRunNumber();

  /**
   * @brief Get the timer of the event
   * @return the timer of the event
   **/
  float GetTimer();

  /**
   * @brief Get the next event from the Tree
   * @return different to 0 if failed
   **/
  int Next();

  /**
   * @brief Get the file containing the Tree.
   * @return TFile containing the Tree
   **/
  void SetScaleFactorTOT(float val);
  void pushvectors(std::vector<float> *vlead, float lead, std::vector<float> *vtot, float tot);
  TFile* GetFile();
  void SetupHistograms();
  void FillHistograms();
  std::vector<TH1D*>* RetrieveHistograms();
  std::vector<TH1D*>* m_vector_histograms;
  void ResetCounters();
 private:
  
  TFile * m_file;
  TTree * m_tree;
  std::chrono::steady_clock::time_point m_t0;
  int resetCounterCorrection =0;
  bool m_resetFromSocket = false;
  uint32_t m_run;
  uint32_t m_maxforpico=0;
  uint32_t m_eventid;
  uint32_t m_eventid_corrected;
  uint32_t m_eventid_previous = 0;
  uint32_t m_eventid_current = 0;
  uint32_t m_channel;
  float m_leading;
  float m_tot;
  uint32_t m_mode;
  float    m_timer;
  float m_scale_factor =3.0;
  std::vector<float> m_channel0_leading;
  std::vector<float> m_channel0_tot;
  std::vector<float> m_channel1_leading;
  std::vector<float> m_channel1_tot;
  std::vector<float> m_channel3_leading;
  std::vector<float> m_channel3_tot;
  std::vector<float> m_channel15_leading;
  std::vector<float> m_channel15_tot;
  int m_resetL1id =0;
  PicoTDCData m_data;
  uint64_t m_entry;
};

#endif

