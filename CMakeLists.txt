############################
# PicoTDC
# Carlos.Solans@cern.ch
############################

tdaq_package()

find_package(ROOT COMPONENTS Tree)

tdaq_add_scripts(share/*.py)

tdaq_add_python_files(python/*.py)

tdaq_add_data(share/PicoTDC.md)

tdaq_add_library(PicoTDC 
                 src/PicoTDC.cpp
                 src/PicoTDCData.cpp
                 src/PicoTDCTree.cpp
                 src/PicoTDCModule.cpp
                 LINK_LIBRARIES 
                 ipbus ROOT MaltaDAQ)

tdaq_add_library(PyPicoTDC
                 src/PyPicoTDC.cxx
                 src/PicoTDC.cpp
                 src/PicoTDCTree.cpp
                 src/PicoTDCData.cpp
                 INCLUDE_DIRECTORIES ${PYTHON_INCLUDE_DIR}
                 OPTIONS -Wno-register
                 LINK_LIBRARIES ipbus ROOT
                 NOINSTALL
                 )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libPyPicoTDC.so 
        DESTINATION ${BINARY_TAG}/lib 
        RENAME PyPicoTDC.so)


tdaq_add_executable(MaltaPicoTDC
                    src/MaltaPicoTDC.cxx
                    src/PicoTDC.cpp 
                    src/PicoTDCModule.cpp 
                    src/PicoTDCData.cpp
                    src/PicoTDCTree.cpp
                    src/PicoTDCAnalysis.cpp
                    LINK_LIBRARIES MaltaDAQ tdaq::cmdline ROOT::Tree ROOT::Hist)
 
tdaq_add_executable(MaltaPicoAnalog  
                    src/MaltaPicoAnalog.cxx
                    LINK_LIBRARIES MaltaDAQ tdaq::cmdline ROOT::Tree ROOT::Hist ROOT::Gpad)

                   
 
